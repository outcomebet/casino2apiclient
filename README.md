# Installation #

Add this repository to composer.json:

```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/outcomebet/casino2apiclient.git"
        }
    ]
}
```

Run installation:

```composer require outcomebet/casino-api-client @dev```

Read more about [Composer](https://getcomposer.org/doc/00-intro.md)

# Usage #

```
#!php
<?php

use outcomebet\casino\api\client\Client;

$client = new Client(array('sslKeyPath' => '/path/to/apikey.pem', 'url' => 'https://api.gamingmodule.com/'));

// check API version
print_r($client->apiVersion());

// register player
print_r($client->setPlayerInfo('user234'));
// or print_r($client->setPlayerInfo(array('player_id' => 'user234', 'nick' => 'user234')));

// check balance
print_r($client->getBalance('user234'));

// deposit balance
print_r($client->changeBalance('user234', 15));

// list games
$games = $client->listGames();
print_r($games);

// run the game
print_r($client->runGame($games[0]['id'], 'user234'));
```